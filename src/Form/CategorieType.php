<?php

namespace App\Form;

use App\Entity\Categorie;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class CategorieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => [
                    'min' => '2',
                    'max' => '100'
                ],
                'constraints' => [
                    new Assert\Positive(),
                    new Assert\Length(['min' => 2, 'max' => 100])
                ]
            ])
            ->add('description',TextareaType::class,[
                'attr' => [
                    'min' => '2',
                    'max' => '255'
                ],
                'constraints' => [
                    new Assert\Positive(),
                    new Assert\Length(['min' => 2, 'max' => 255])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Categorie::class,
        ]);
    }
}
