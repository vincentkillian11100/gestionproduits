<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\Categorie;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => [
                    'min' => '2',
                    'max' => '100'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 2, 'max' => 100])
                ]
            ]) 
            ->add('description',TextareaType::class,[
                'attr' => [
                    'min' => '2',
                    'max' => '255'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 2, 'max' => 255])
                ]
            ])
            ->add('quantity', IntegerType::class, [
                'attr' => [
                    'min' => '2',
                    'max' => '100'
                ],
                'constraints' => [
                    new Assert\Positive(),
                    new Assert\Length(['min' => 2, 'max' => 100])
                ]
            ])
            ->add('price', IntegerType::class, [
                'attr' => [
                    'min' => '2',
                    'max' => '100'
                ],
                'constraints' => [
                    new Assert\Positive(),
                    new Assert\Length(['min' => 2, 'max' => 100])
                ]
            ])
            ->add('datecreate', DateTimeType::class,[
                'years' => [2022,2023],
                'placeholder' =>[
                    'year' => 'Year', 'month' => 'Month', 'day' => 'Day',
                    'hour' => 'Hour', 'minute' => 'Minute', 'second' => 'Second',
                ],
                'hours' => [12,13,14,19, 20, 21, 23,]
            ])
            ->add('categorie', EntityType::class, [
                'label' => 'Catégorie',
                'class' => Categorie::class,
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'form-control mb-2',
                    'required' => false,
                ]])
            // ->add('author')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
