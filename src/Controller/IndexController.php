<?php 

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\App\Controller\ProductRepository;

#[Route('/')]
class IndexController extends AbstractController
{
    #[Route('/', name: 'index_index',methods:['GET'])]
    public function index()
    {

        return $this->render('base.html.twig');
    }
}








?>